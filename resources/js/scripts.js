//alert('ok');
 
//  objeto JSON com os dados
var dados = [
  {
    pilha: "Pilha Alcalina Duracell (AA)",
    tensao_nominal: 1.5,
    corrente: 2800,
    E: 1.304,
    V: 1.286,
    R: 23.7,
  },
  {
    pilha: "Pilha Duracell (AAA)",
    tensao_nominal: 1.5,
    corrente: 1200,
    E: 0.988,
    V: 0.820,
    R: 23.7,
  },
  {
    pilha: "Bateria Golite",
    tensao_nominal: 9.0,
    corrente: 500,
    E: 5.53,
    V: 0.00553,
    R: 23.7,
  },
  {
    pilha: "Bateria Elgin",
    tensao_nominal: 9.0,
    corrente: 250,
    E: 7.19,
    V: 0.00208,
    R: 23.7,
  },
  {
    pilha: "Pilha JYX (recarregável)",
    tensao_nominal: 3.7,
    corrente: 9800,
    E: 2.484,
    V: 2.338,
    R: 23.7,
  },
  {
    pilha: "Pilha Panasonic (AA)",
    tensao_nominal: 1.5,
    corrente: 2500,
    E: 1.378,
    V: 1.336,
    R: 23.7,
  },
  {
    pilha: "Pilha Philips (AAA)",
    tensao_nominal: 5,
    corrente: 1200,
    E: 1.368,
    V: 1.307,
    R: 23.7,
  },
  {
    pilha: "Pilha Luatek",
    tensao_nominal: 3.7,
    corrente: 1200,
    E: 2.410,
    V: 2.336,
    R: 23.7,
  },
  {
    pilha: "Bateria Fredoom",
    tensao_nominal: 12,
    corrente: 26000,
    E: 10.68,
    V: 10.60,
    R: 23.7,
  },
  {
    pilha: "Bateria Unipower",
    tensao_nominal: 12,
    corrente: 7000,
    E: 10.48,
    V: 10.32,
    R: 23.7,
  },

];
 
// Função para calcular a resistencia interna de cada medição e preencher a tabela
function calcularResistencia() {
  var tabela = document.getElementById("tbDados");
 
  // Limpar tabela antes de preencher
  var tabelaHTML = `<tr>
    <th>Pilha/Bateria</th>
    <th>Tensão Nominal (V)</th>
    <th>Capacidade de corrente (mA.h)</th>
    <th>Tensão sem carga(V)</th>
    <th>Tensão com carga(E)</th>
    <th>Resitência de carga(Ohm)</th>
    <th>Resistência Interna(Ohm)</th>
  </tr>`;
 
  // Iterar sobre os dados e calcular o r de cada um
  for (var i = 0; i < dados.length; i++) {
    var linha = dados[i];
    var E = linha.E;
    var V = linha.V;
    var R = linha.R;
    var r = R * (E / V - 1);
 
    // Adicionar nova linha à tabela com os valores e a soma
    var novaLinha = "<tr><td>" + linha.pilha + "</td><td>" + linha.tensao_nominal + "</td><td>" + linha.corrente + "</td><td>" + linha.E + "</td><td>" + linha.V + "</td><td>" + linha.R + "</td><td>" + r.toFixed(4) + "</td></tr>";
 
    tabelaHTML += novaLinha;
  }
  tabela.innerHTML = tabelaHTML;
}
 
calcularResistencia();
